<?php

namespace AppBundle\Controller;


use AppBundle\Document\Notification;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ClientController
 * @package AppBundle\Controller
 *
 * @Route("/notification")
 */
class NotificationController extends Controller {

    /**
     * @Route("/", name="app_notification_index", options={"expose"=true})
     * @Method({"GET"})
     */
    public function indexAction(Request $request) {

        $dm = $this->get('doctrine_mongodb')->getManager();

        $query = $dm->createQueryBuilder('AppBundle:Notification')->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            1/*limit per page*/
        );

        return $this->render(':Notification:index.html.twig', array(
            'pagination' => $pagination,
            'page' => 1,
        ));

    }

    public function testCreationAction(){

        $dm = $this->get('doctrine_mongodb')->getManager();

        $notication1 = new Notification();
        $notication1->setTopicCode('sp-sign-up');
        $notication1->setTopicDescripction('Service Provider Sign Up');
        $notication1->setMessage('Russell M. Nelson was joined to Acclink with his Company Russell Technologies Inc.');
        $notication1->setAction('link-to-company-profile');
        $notication1->setLink('http://acclink.com/admin/service-provider/company/12342');
        $notication1->setStatus('unread');
        $notication1->setCreationDate(new \DateTime());
        $notication1->setCreatedBy([
            'id'=> 2000,
            'username'=> "rusellmnelson",
            'name'=> "Russell M. Nelson",
            'email'=> "rusellmnelson@rusell.com",
        ]);

        $notication2 = new Notification();
        $notication2->setTopicCode('sp-sign-up');
        $notication2->setTopicDescripction('Service Provider Sign Up');
        $notication2->setMessage('Russell M. Nelson was joined to Acclink with his Company Russell Technologies Inc.');
        $notication2->setAction('link-to-company-profile');
        $notication2->setLink('http://acclink.com/admin/service-provider/company/12342');
        $notication2->setStatus('unread');
        $notication2->setCreationDate(new \DateTime());
        $notication2->setCreatedBy([
            'id'=> 2000,
            'username'=> "rusellmnelson",
            'name'=> "Russell M. Nelson",
            'email'=> "rusellmnelson@rusell.com",
        ]);

        $dm->persist($notication1);

        $notication2 = new Notification();
        $notication2->setTopicCode('sp-sign-up');
        $notication2->setTopicDescripction('Service Provider Sign Up');
        $notication2->setMessage('Russell M. Nelson was joined to Acclink with his Company Russell Technologies Inc.');
        $notication2->setAction('link-to-company-profile');
        $notication2->setLink('http://acclink.com/admin/service-provider/company/12342');
        $notication2->setStatus('read');
        $notication2->setCreationDate(new \DateTime());
        $notication2->setReadedBy([
            [
                'id' => 2,
                'username' => 'jkatzenbach',
                'name' => 'John Katzenbach',
                'email' => 'jkatzenbach@accelerance.com',
                'date' => '2016-08-24 10:35:48'
            ],
            [
                'id' => 1,
                'username' => 'admin',
                'name' => 'Steve Mezak',
                'email' => 'smezak@accelerance.com',
                'date' => '2016-08-24 10:45:51'
            ],
        ]);

        $dm->persist($notication2);
        $dm->flush();

        return $this->render(':Notification:index.html.twig');

    }

}
