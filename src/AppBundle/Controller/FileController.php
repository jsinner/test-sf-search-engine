<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Form\FileFormType;
use AppBundle\Form\FileSearchFormType;
use AppBundle\Form\SearchFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class FileController
 * @package AppBundle\Controller
 *
 * @Route("/file")
 */
class FileController extends Controller {

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/register", name="app_file_register", options={"expose"=true})
     * @Method({"GET","POST"})
     */
    public function registerAction(Request $request) {

        /** @var File $file */
        $file = new File();

        $form = $this->createForm(FileFormType::class, $file);
        $form->handleRequest($request);

        // dump($form->isSubmitted(), $request->getMethod());

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($file);
            $em->flush();

            $this->addFlash('success', 'The file was updated sucessfully.');
            return $this->redirectToRoute('app_file_search');
        }

        return $this->render(':File:register.html.twig',
            [
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/search", name="app_file_search", options={"expose"=true})
     * @Method({"GET"})
     */
    public function searchAction(Request $request){

        /** @var SearchFile $file */
        $file = new SearchFile();

        $form = $this->createForm(FileSearchFormType::class, $file);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setIgnoredAttributes(array('content',));
        $encoder = new JsonEncoder();
        $serializer = new Serializer(array($normalizer), array($encoder));

        if ($form->isSubmitted() || $request->isXmlHttpRequest()){
            $filesListObj = $em->getRepository('AppBundle:File')->search($file, $this->getParameter('uploaded_file_directory'));
            $filesListJson = $serializer->serialize($filesListObj, 'json');
            $response = array(
                'result' => 'success',
                'filesList' => json_decode($filesListJson),
            );
            return new JsonResponse($response);
        }

        $filesListObj = $em->getRepository('AppBundle:File')->findAll();
        $filesListJson = $serializer->serialize($filesListObj, 'json');

        return $this->render(':File:search.html.twig',
            [
                'form' => $form->createView(),
                'filesList' => $filesListJson,
            ]
        );

    }

}
