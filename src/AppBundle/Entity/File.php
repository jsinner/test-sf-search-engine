<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * File
 *
 * @ORM\HasLifecycleCallbacks()
 *
 * @ORM\Table(name="file")
 * @UniqueEntity(fields={"title", "location"})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileRepository")
 */
class File
{
    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Length(max="100")
     *
     * @ORM\Column(name="title", type="string", length=100, unique=true)
     */
    private $title;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please, upload a plain text file.")
     * @Assert\File(
     *     mimeTypes={ "text/plain", "text/csv", "text/html", "application/xml", "text/javascript", "application/json" },
     *     maxSize="100M")
     *
     * @ORM\Column(name="location", type="string", length=255, unique=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     *
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FileType", inversedBy="files")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return File
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return File
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return File
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set type
     *
     * @param FileType $type
     *
     * @return File
     */
    public function setType(FileType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return FileType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return File
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return File
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValues() {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getTitle();
    }
}

