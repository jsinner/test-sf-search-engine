<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileSearchFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('title', TextType::class, array(
                'required' => false
            ))
            ->add('description', TextType::class, array(
                'required' => false
            ))
            ->add('content', TextType::class, array(
                'required' => false
            ))
            ->add('createdAt', TextType::class, array(
                'required' => false
            ))
            ->add('type', EntityType::class, array(
                'class' => 'AppBundle:FileType',
                'choice_label' => 'description',
                'required' => false,
                'empty_data'  => null,
            ))
            ->add('searchType',  ChoiceType::class, array(
                'choices'  => array(
                    'Using DQL' => 'DQL',
                    'Using Native SQL' => 'SQL',
                    'In File System' => 'FS',
                ),
                'required' => true
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Form\SearchFile'
        ));
    }
}
