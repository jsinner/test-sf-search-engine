<?php
/**
 * Created by PhpStorm.
 * User: jsinner
 * Date: 25/07/16
 * Time: 02:10 PM
 */

namespace AppBundle\Command;

use AppBundle\Entity\FileType;
use AppBundle\Form\SearchFile;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class SearchFileCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('sinner:search:file')
            ->setDescription('Search Files by its contents')
            ->addArgument(
                'storage',
                InputArgument::OPTIONAL,
                'Where do you want to make the search? (database|filesystem): '
            )
            ->addArgument(
                'type',
                InputArgument::OPTIONAL,
                'What do you want to use to make the search? (dql|sql): ',
                'dql'
            )
            ->addOption(
                'title',
                null,
                InputOption::VALUE_OPTIONAL,
                'Title of File: '
            )
            ->addOption(
                'description',
                null,
                InputOption::VALUE_OPTIONAL,
                'Description of File: '
            )
            ->addOption(
                'content',
                null,
                InputOption::VALUE_OPTIONAL,
                'Content of File: '
            )
            ->addOption(
                'createdAt',
                null,
                InputOption::VALUE_OPTIONAL,
                'File Creation Date (YYYY-MM-DD): '
            )
            ->addOption(
                'fileType',
                null,
                InputOption::VALUE_OPTIONAL,
                'File Extension (txt|md|js|html): '
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $storage = $input->getArgument('storage');

        if(is_null($storage) || strlen($storage)===0){
            $this->searchInDatabase($input, $output);
        }
        elseif(in_array($storage, array('database', 'db'))) {
            $this->searchInDatabase($input, $output);
        }
        elseif(in_array($storage, array('filesystem', 'fs'))) {
            $this->searchInFileSystem($input, $output);
        }
        else {
            $output->writeln('<error>foo</error>');
            die();
        }

        $output->writeln('... ');
        $output->writeln(' ');
        $output->writeln('<comment>Thanks for use My File Search Engine</comment>');
    }

    protected function searchInDatabase(InputInterface $input, OutputInterface $output){
        /** @var SearchFile $file */
        $file = $file = new SearchFile();

        $fileType = (in_array($input->getOption('fileType'), array('txt','md', 'js', 'html')))?$input->getOption('fileType'):null;
        $searchType = (in_array($input->getArgument('type'), array('dql', 'sql')))?$input->getArgument('type'):'';

        $file->setTitle($input->getOption('title'));
        $file->setDescription($input->getOption('description'));
        $file->setCreatedAt($input->getOption('createdAt'));
        $file->setContent($input->getOption('content'));

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $fileTypeData = $this->getContainer()->get('app.file_uploader')->getMimeType($fileType);
        if($fileTypeData){
            $fileType = $em->getReference('AppBundle:FileType', $fileTypeData['id']);
            $fileType->setName($fileTypeData['mime']);
            $file->setType($fileType);
        }

        $searchType = (strlen($searchType)>0)?strtoupper($input->getArgument('type')):'DQL';
        $file->setSearchType($searchType);

        $filesListObj = $em->getRepository('AppBundle:File')->search($file, $this->getContainer()->getParameter('uploaded_file_directory'));

        $filesData = [];

        if(count($filesListObj)) {
            foreach ($filesListObj as $fileData) {
                $filesData[] = [
                    $fileData->getId(),
                    $fileData->getTitle(),
                    $fileData->getType()->getName(). ' - ' .$fileData->getType()->getDescription(),
                    $fileData->getCreatedAt()->format('Y-m-d H:i:s'),
                    $this->getContainer()->getParameter('uploaded_file_directory_local').$fileData->getLocation(),
                ];
            }
        }

        $this->printTable($output, $filesData);

    }

    protected function searchInFileSystem(InputInterface $input, OutputInterface $output){

        $finder = new Finder();
        $finder->files()->in($this->getContainer()->getParameter('uploaded_file_directory'));

        $files = [];

        $content = $input->getOption('content');

        $finder->files()->contains("$content");

        foreach ($finder as $file) {
            $files[] = array($file->getRealPath());
        }

        $table = new Table($output);
        $table
            ->setHeaders(array('File Path'))
            ->setRows(
                $files
            )
        ;
        $table->render();

    }

    protected function printTable(OutputInterface $output, $filesData) {
        if(count($filesData)>0){
            $table = new Table($output);
            $table
                ->setHeaders(array('ID', 'Title', 'Type', 'Creation Date', 'File Path'))
                ->setRows(
                    $filesData
                )
            ;
            $table->render();
        }
        else{
            $output->writeln('<comment>There is no files that apply to this parameters</comment>');
        }
    }

}