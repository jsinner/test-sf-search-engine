<?php

namespace AppBundle\Twig\Extension;

/**
 *
 *
 * Class AppExtension
 * @package AppBundle\Twig\Extension
 */
class AppExtension extends \Twig_Extension{

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('json_decode', array($this, 'jsonDecode')),
            new \Twig_SimpleFilter('addslashes', array($this, 'addSlashes')),
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
        );
    }

    public function jsonDecode($str) {
        return json_decode($str, true);
    }

    public function addSlashes($str){
        return addslashes($str);
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }

    public function getName() {
        return 'app_extension';
    }

}