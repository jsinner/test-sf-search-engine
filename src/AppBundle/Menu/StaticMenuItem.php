<?php

namespace AppBundle\Menu;

use AppBundle\Menu\MenuItemInterface;

class StaticMenuItem implements MenuItemInterface{

    private $ordenLista;

    private $codigo;

    private $etiqueta;

    private $sfRoute;

    private $icono;

    private $etiquetaItemParent;

    private $loadByAjax;

    private $esExterno;

    private $urlExterna;

    /**
     * StaticMenuItem constructor.
     * @param $codigo
     * @param $etiqueta
     * @param $sfRoute
     * @param $icono
     * @param $etiquetaItemParent
     * @param $loadByAjax
     * @param $esExterno
     * @param $urlExterna
     */
    public function __construct($ordenLista, $codigo, $etiqueta, $icono, $sfRoute=null, $etiquetaItemParent=null, $loadByAjax=false, $esExterno=false, $urlExterna=null){
        $this->ordenLista = $ordenLista;
        $this->codigo = $codigo;
        $this->etiqueta = $etiqueta;
        $this->sfRoute = $sfRoute;
        $this->icono = $icono;
        $this->etiquetaItemParent = $etiquetaItemParent;
        $this->loadByAjax = $loadByAjax;
        $this->esExterno = $esExterno;
        $this->urlExterna = $urlExterna;
    }

    /**
     * @return mixed
     */
    public function getOrdenLista()
    {
        return $this->ordenLista;
    }

    /**
     * @param mixed $ordenLista
     * @return StaticMenuItem
     */
    public function setOrdenLista($ordenLista)
    {
        $this->ordenLista = $ordenLista;
        return $this;
    }

    /**
     * @return null
     */
    public function getUrlExterna()
    {
        return $this->urlExterna;
    }

    /**
     * @param null $urlExterna
     * @return StaticMenuItem
     */
    public function setUrlExterna($urlExterna)
    {
        $this->urlExterna = $urlExterna;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     * @return StaticMenuItem
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEtiqueta()
    {
        return $this->etiqueta;
    }

    /**
     * @param mixed $etiqueta
     * @return StaticMenuItem
     */
    public function setEtiqueta($etiqueta)
    {
        $this->etiqueta = $etiqueta;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSfRoute()
    {
        return $this->sfRoute;
    }

    /**
     * @param mixed $sfRoute
     * @return StaticMenuItem
     */
    public function setSfRoute($sfRoute)
    {
        $this->sfRoute = $sfRoute;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEsExterno()
    {
        return $this->esExterno;
    }

    /**
     * @param mixed $esExterna
     * @return StaticMenuItem
     */
    public function setEsExterno($esExterno)
    {
        $this->esExterno = $esExterno;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcono()
    {
        return $this->icono;
    }

    /**
     * @param mixed $icono
     * @return StaticMenuItem
     */
    public function setIcono($icono)
    {
        $this->icono = $icono;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEtiquetaItemParent()
    {
        return $this->etiquetaItemParent;
    }

    /**
     * @param mixed $codigoItemParent
     * @return StaticMenuItem
     */
    public function setEtiquetaItemParent($etiquetaItemParent)
    {
        $this->etiquetaItemParent = $etiquetaItemParent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLoadByAjax()
    {
        return $this->loadByAjax;
    }

    /**
     * @param mixed $loadByAjax
     * @return StaticMenuItem
     */
    public function setLoadByAjax($loadByAjax)
    {
        $this->loadByAjax = $loadByAjax;
        return $this;
    }

}