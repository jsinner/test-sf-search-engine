<?php

namespace AppBundle\Menu;

use AppBundle\Menu\StaticMenuItem;
use Knp\Menu\FactoryInterface;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuBuilder implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;


    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * MenuBuilder constructor.
     * @param FactoryInterface $factory
     * @param RequestStack $requestStack
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(FactoryInterface $factory, RequestStack $requestStack, AuthorizationCheckerInterface $authorizationChecker){
        $this->factory = $factory;
        $this->requestStack = $requestStack;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function createMainMenu(array $options=array()){

        // if (false === $this->authorizationChecker->isGranted('ROLE_NEWSLETTER_ADMIN')) {
        //     throw new AccessDeniedException();
        // }
        $factory = $this->factory;
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav-list');

        /**
         * @var StaticMenuItem[]
         */
        $menuItems = $this->getStaticMenuItems();

        foreach ($menuItems as $menuItem) {

            $menuItemCode = trim($menuItem->getCodigo());
            $menuItemName  = trim($menuItem->getEtiqueta());
            $menuItemRoute = trim($menuItem->getSfRoute());
            $menuItemIcon  = trim($menuItem->getIcono());

            if(null == $menuItem->getEtiquetaItemParent()){
                //El Elemento es un Contenedor
                $menu->addChild($menuItemName, array('route' => $menuItemRoute))
                     ->setAttribute('icon', $menuItemIcon)
                     ->setAttribute('container', true)
                     ->setAttribute('id', "main-menu-$menuItemCode");
            }else{
                
                $itemParent = $menuItem->getEtiquetaItemParent();
                if($itemParent instanceof MenuItemInterface){
                    $itemParentName = $itemParent->getEtiqueta();
                }
                else{
                    $itemParentName = $itemParent;
                }

                $menu[$itemParentName]->addChild($menuItemName, array('route' => $menuItemRoute,))
                                      ->setAttribute('class', 'submenu')
                                      ->setAttribute('icon', (strlen($menuItemIcon)>0)?$menuItemIcon:'fa fa-double-angle-right')
                                      ->setAttribute('title', $menuItemName)
                                      ->setAttribute('id', "main-menui-$menuItemCode");

            }
            
        }
         
        return $menu;
    }

    private function getStaticMenuItems(){

        $session = $this->requestStack->getCurrentRequest()->getSession();

        $menuItems = $session->get('menu');

        if(!$menuItems){

            $menuItems = array(
                new StaticMenuItem(0, 'home', 'Home', 'fa fa-lg fa-fw fa-home', 'homepage'),
                new StaticMenuItem(1, 'files', 'Files', 'fa fa-lg fa-fw fa-lock'),
                    new StaticMenuItem(2, 'upload-file', 'Upload File', 'fa fa-lg fa-fw fa-tags', 'app_file_register', 'Files'),
                    new StaticMenuItem(3, 'search-files', 'Search File', 'fa fa-lg fa-fw fa-tags', 'app_file_search', 'Files'),
                new StaticMenuItem(4, 'files', 'Notifications', 'fa fa-lg fa-fw fa-group'),
                    new StaticMenuItem(5, 'notifications-list', 'List', 'fa fa-lg fa-fw fa-group', 'app_notification_index', 'Notifications'),
            );

            // $session->set('menu', $menuItems);
        }

        return $menuItems;

    }

}

