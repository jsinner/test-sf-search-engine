<?php
/**
 * Created by PhpStorm.
 * User: jsinner
 * Date: 21/05/16
 * Time: 06:51 PM
 */

namespace AppBundle\Menu;


interface MenuItemInterface
{
    public function getOrdenLista();
    public function getCodigo();
    public function getEtiqueta();
    public function getSfRoute();
    public function getEsExterno();
    public function getUrlExterna();
    public function getIcono();
    public function getEtiquetaItemParent();
}