<?php

/**
 * @author Jose Gabriel Gonzalez <jgabrielsinner10@gmail.com>
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="notification")
 */
class Notification {

    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $topicCode;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $topicDescripction;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $message;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $action;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $link;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $creationDate;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $createdBy;

    /**
     * @MongoDB\Field(type="collection")
     */
    protected $readedBy;

    


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set topicCode
     *
     * @param string $topicCode
     * @return $this
     */
    public function setTopicCode($topicCode)
    {
        $this->topicCode = $topicCode;
        return $this;
    }

    /**
     * Get topicCode
     *
     * @return string $topicCode
     */
    public function getTopicCode()
    {
        return $this->topicCode;
    }

    /**
     * Set topicDescripction
     *
     * @param string $topicDescripction
     * @return $this
     */
    public function setTopicDescripction($topicDescripction)
    {
        $this->topicDescripction = $topicDescripction;
        return $this;
    }

    /**
     * Get topicDescripction
     *
     * @return string $topicDescripction
     */
    public function getTopicDescripction()
    {
        return $this->topicDescripction;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * Get action
     *
     * @return string $action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Get link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set creationDate
     *
     * @param timestamp $creationDate
     * @return $this
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return timestamp $creationDate
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set createdBy
     *
     * @param hash $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return hash $createdBy
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set readedBy
     *
     * @param collection $readedBy
     * @return $this
     */
    public function setReadedBy($readedBy)
    {
        $this->readedBy = $readedBy;
        return $this;
    }

    /**
     * Get readedBy
     *
     * @return collection $readedBy
     */
    public function getReadedBy()
    {
        return $this->readedBy;
    }

    public function __toString()
    {
        return $this->topicDescripction.': '.$this->message.'. ('.$this->creationDate.')';
    }

}
