<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader {

    private $targetDir;

    public function __construct($targetDir) {
        $this->targetDir = $targetDir;
    }

    public function upload(UploadedFile $file, $title) {
        $fileName = md5(uniqid($title.date('Y-m-d H:i:s'), true)).'.'.$file->guessExtension();

        $file->move($this->targetDir, $fileName);

        return $fileName;
    }

    public function getHumanFileSize($bytes, $decimals = 2) {

        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];

    }

    public function getMimeType($mimeType){

        $fileTypes = [
            ['id'=>1, 'extension'=>'txt', 'mime'=>'text/plain'],
            ['id'=>1, 'extension'=>'txt', 'mime'=>'application/octet-stream'],
            ['id'=>2, 'extension'=>'csv', 'mime'=>'text/csv'],
            ['id'=>3, 'extension'=>'htm', 'mime'=>'text/html'],
            ['id'=>3, 'extension'=>'html', 'mime'=>'text/html'],
            ['id'=>4, 'extension'=>'xml', 'mime'=>'application/xml'],
            ['id'=>5, 'extension'=>'js', 'mime'=>'application/javascript'],
            ['id'=>6, 'extension'=>'json', 'mime'=>'application/json'],
            ['id'=>7, 'extension'=>'md', 'mime'=>'text/x-markdown'],
        ];

        $fileTypeResult = null;

        foreach ($fileTypes as $type){
            if ($type['mime']==$mimeType || $type['extension']==$mimeType){
                $fileTypeResult = $type;
                break;
            }
        }

        return $fileTypeResult;

    }

    /**
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

}