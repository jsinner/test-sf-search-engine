<?php
/**
 * Created by PhpStorm.
 * User: jsinner
 * Date: 24/07/16
 * Time: 10:34 AM
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\FileType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use AppBundle\Entity\File;
use AppBundle\Service\FileUploader;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class FileUploadListener {
    
    /** @var FileUploader */
    private $uploader;
    
    /** @var EntityManager */
    private $em;

    public function __construct(FileUploader $uploader) {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $this->em = $args->getEntityManager();
        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args) {
        $entity = $args->getEntity();
        $this->em = $args->getEntityManager();
        $this->uploadFile($entity);
    }

    private function uploadFile($entity) {

        // upload only works for File entities
        if (!$entity instanceof File) {
            return;
        }

        /** @var UploadedFile $file */
        $file = $entity->getLocation();
        
        // only upload new files
        if (!$file instanceof UploadedFile) {
            return;
        }

        $fileName = $this->uploader->upload($file, $entity->getTitle());

        $entity->setLocation($fileName);
        $entity->setSize($file->getClientSize());
        
        $fileTypeData = $this->uploader->getMimeType($file->getExtension());
        $fileTypeId = (isset($fileTypeData['id']))?$fileTypeData['id']:null;

        if ($fileTypeId==null) {
            throw new InvalidParameterException('The file type '.$file->getClientMimeType().' with extension *.'.$file->getExtension().' is not allowed.');
        }

        $fileType = $this->em->getReference('AppBundle:FileType', $fileTypeId);
        $fileType->setName($fileTypeData['mime']);

        $entity->setType($fileType);
        $entity->setContent(file_get_contents($this->uploader->getTargetDir().'/'.$fileName));

    }
}