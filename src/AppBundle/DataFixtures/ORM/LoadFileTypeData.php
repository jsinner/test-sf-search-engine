<?php
/**
 * Created by PhpStorm.
 * User: jsinner
 * Date: 23/07/16
 * Time: 08:18 AM
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\FileType;

/**
 * Class LoadFileTypeData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadFileTypeData implements FixtureInterface {

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $data = [
            [
                'name'=>'text/plain',
                'description'=>'Text File',
                'extension'=>'txt',
            ],
            [
                'name'=>'text/csv',
                'description'=>'CSV File',
                'extension'=>'csv',
            ],
            [
                'name'=>'text/html',
                'description'=>'HTML File',
                'extension'=>'html',
            ],
            [
                'name'=>'application/xml',
                'description'=>'XML File',
                'extension'=>'xml',
            ],
            [
                'name'=>'text/javascript',
                'description'=>'Javascript File',
                'extension'=>'js',
            ],
            [
                'name'=>'application/json',
                'description'=>'JSON File',
                'extension'=>'json',
            ],
            [
                'name'=>'text/x-markdown',
                'description'=>'Markdown File',
                'extension'=>'md',
            ]
        ];

        foreach ($data as $fileType) {
            $userAdmin = new FileType();
            $userAdmin->setName($fileType['name']);
            $userAdmin->setDescription($fileType['description']);
            $userAdmin->setExtension($fileType['extension']);
            $manager->persist($userAdmin);
        }

        $manager->flush();
    }
}
