Welcome to the Doctrine Project

The Doctrine Project is the home to several PHP libraries primarily focused on database storage and object mapping. The core projects are a Object Relational Mapper (ORM) and the Database Abstraction Layer (DBAL) it is built upon. Doctrine has greatly benefited from concepts of the Hibernate ORM and has adapted them to fit the PHP language.
Why use Doctrine?

    Around since 2006 with very stable, high-quality codebase.
    Extremely flexible and powerful object-mapping and query features.
    Support for both high-level and low-level database programming for all your use-cases.
    Large Community and integrations with many different frameworks (Symfony, Zend Framework, CodeIgniter, Flow, Lithium and more)