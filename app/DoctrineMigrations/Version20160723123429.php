<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160723123429 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX UNIQ_8C9F36105E9E89CB');
        $this->addSql('ALTER TABLE file ALTER id DROP DEFAULT');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8C9F36102B36786B ON file (title)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8C9F36105E9E89CB ON file (location)');
        $this->addSql('ALTER TABLE file_type ALTER id DROP DEFAULT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE file_type_id_seq');
        $this->addSql('SELECT setval(\'file_type_id_seq\', (SELECT MAX(id) FROM file_type))');
        $this->addSql('ALTER TABLE file_type ALTER id SET DEFAULT nextval(\'file_type_id_seq\')');
        $this->addSql('DROP INDEX UNIQ_8C9F36102B36786B');
        $this->addSql('DROP INDEX uniq_8c9f36105e9e89cb');
        $this->addSql('CREATE SEQUENCE file_id_seq');
        $this->addSql('SELECT setval(\'file_id_seq\', (SELECT MAX(id) FROM file))');
        $this->addSql('ALTER TABLE file ALTER id SET DEFAULT nextval(\'file_id_seq\')');
        $this->addSql('CREATE UNIQUE INDEX uniq_8c9f36105e9e89cb ON file (location, title)');
    }
}
