<?php
/**
 * Created by PhpStorm.
 * User: jsinner
 * Date: 25/07/16
 * Time: 03:13 PM
 */

namespace tests\AppBundle\Command;

use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AppBundle\Command\SearchFileCommand;

class SearchFileCommandTest extends KernelTestCase {

    public function testWithoutParamsExecute() {

        $kernel = $this->createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new SearchFileCommand());

        $command = $application->find('sinner:search:file');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array());

        $this->assertRegExp("/[Symfony, React, Angular, Doctrine]/", $commandTester->getDisplay());

    }

    public function testWithTitleParamsExecute() {

        $kernel = $this->createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new SearchFileCommand());

        $command = $application->find('sinner:search:file');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            '--title'=>'Symfony'
        ));

        $this->assertRegExp("/[Symfony]/", $commandTester->getDisplay());

    }

    public function testWithContentFileSystemParamsExecute() {

        $kernel = $this->createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new SearchFileCommand());

        $command = $application->find('sinner:search:file');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'storage' => 'fs',
            '--content'=>'Symfony'
        ));

        $this->assertRegExp("/[uploads]/", $commandTester->getDisplay());

    }

}