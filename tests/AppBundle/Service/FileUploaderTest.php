<?php

/**
 * @author José Gabriel González <jgabrielsinner10@gmail.com>
 */
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FileUploaderTest extends WebTestCase {

    public function testIndex() {

        $client = static::createClient();
        $container = $client->getContainer();
        
        $fileUploader = $container->get('app.file_uploader');

        $mimeType = $fileUploader->getMimeType('text/plain');
        $this->assertEquals(1, $mimeType['id']);

        $mimeType = $fileUploader->getMimeType('text/x-markdown');
        $this->assertEquals(7, $mimeType['id']);

        $mimeType = $fileUploader->getMimeType('application/pdf');
        $this->assertEquals(null, $mimeType);
        
    }
}
