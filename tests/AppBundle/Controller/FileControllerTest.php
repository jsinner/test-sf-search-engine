<?php

/**
 * @author José Gabriel González <jgabrielsinner10@gmail.com>
 */
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileControllerTest extends WebTestCase {

    public function testRegisterGet() {

        $client = static::createClient();

        $crawler = $client->request('GET', '/file/register');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Register new File")')->count());
        $this->assertContains('file_form', $crawler->filter('form.smart-form')->attr('name'));
    }

    public function testRegisterOnePost() {

        $client = static::createClient();
        $container = $client->getContainer();

        $crawler = $client->request('GET', '/file/register');

        $buttonCrawlerNode = $crawler->selectButton('Submit');

        $form = $buttonCrawlerNode->form();

        $filesDirectoryToTest = $container->getParameter('uploaded_file_directory_test');

        // File 1
        $file = new UploadedFile(
            $filesDirectoryToTest.'FILE-1.txt',
            'FILE-1.txt',
            'text/plain',
            filesize($filesDirectoryToTest.'FILE-1.txt'),
            null,
            true
        );

        $client->submit($form, array(
            'file_form[title]' => 'Angular js',
            'file_form[description]' => 'A file that talk about Angularjs',
            'file_form[location]' => $file
        ));

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

    }

    public function testRegisterTwoPost() {

        $client = static::createClient();
        $container = $client->getContainer();

        $crawler = $client->request('GET', '/file/register');

        $buttonCrawlerNode = $crawler->selectButton('Submit');

        $form = $buttonCrawlerNode->form();

        $filesDirectoryToTest = $container->getParameter('uploaded_file_directory_test');

        // File 2
        $file = new UploadedFile(
            $filesDirectoryToTest.'FILE-2.txt',
            'FILE-2.txt',
            'text/plain',
            filesize($filesDirectoryToTest.'FILE-2.txt'),
            null,
            true
        );

        $client->submit($form, array(
            'file_form[title]' => 'Symfony',
            'file_form[description]' => 'A file that talk about Symfony',
            'file_form[location]' => $file
        ));

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

    }

    public function testRegisterThreePost() {

        $client = static::createClient();
        $container = $client->getContainer();

        $crawler = $client->request('GET', '/file/register');

        $buttonCrawlerNode = $crawler->selectButton('Submit');

        $form = $buttonCrawlerNode->form();

        $filesDirectoryToTest = $container->getParameter('uploaded_file_directory_test');

        // File 3
        $file = new UploadedFile(
            $filesDirectoryToTest.'FILE-3.md',
            'FILE-3.md',
            'text/x-markdown',
            filesize($filesDirectoryToTest.'FILE-3.md'),
            null,
            true
        );

        $client->submit($form, array(
            'file_form[title]' => 'Reactjs',
            'file_form[description]' => 'A file that talk about Reactjs',
            'file_form[location]' => $file
        ));

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

    }

    public function testRegisterFourPost() {

        $client = static::createClient();
        $container = $client->getContainer();

        $crawler = $client->request('GET', '/file/register');

        $buttonCrawlerNode = $crawler->selectButton('Submit');

        $form = $buttonCrawlerNode->form();

        $filesDirectoryToTest = $container->getParameter('uploaded_file_directory_test');

        // File 4
        $file = new UploadedFile(
            $filesDirectoryToTest.'FILE-4.md',
            'FILE-4.md',
            'text/x-markdown',
            filesize($filesDirectoryToTest.'FILE-4.md'),
            null,
            true
        );

        $client->submit($form, array(
            'file_form[title]' => 'Doctrine ORM',
            'file_form[description]' => 'A file that talk about Doctrine ORM',
            'file_form[location]' => $file
        ));

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

    }

    public function testRegisterFivePost() {

        $client = static::createClient();
        $container = $client->getContainer();

        $crawler = $client->request('GET', '/file/register');

        $buttonCrawlerNode = $crawler->selectButton('Submit');

        $form = $buttonCrawlerNode->form();

        $filesDirectoryToTest = $container->getParameter('uploaded_file_directory_test');

        // File 5
        $file = new UploadedFile(
            $filesDirectoryToTest.'FILE-5.txt',
            'FILE-5.txt',
            'text/plain',
            filesize($filesDirectoryToTest.'FILE-5.txt'),
            null,
            true
        );

        $client->submit($form, array(
            'file_form[title]' => 'Node.js',
            'file_form[description]' => 'A file that talk about node.js (nodejs)',
            'file_form[location]' => $file
        ));

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

    }

    public function testRegisterSixPost() {

        $client = static::createClient();
        $container = $client->getContainer();

        $crawler = $client->request('GET', '/file/register');

        $buttonCrawlerNode = $crawler->selectButton('Submit');

        $form = $buttonCrawlerNode->form();

        $filesDirectoryToTest = $container->getParameter('uploaded_file_directory_test');

        // File 6
        $file = new UploadedFile(
            $filesDirectoryToTest.'FILE-6.txt',
            'FILE-6.txt',
            'text/plain',
            filesize($filesDirectoryToTest.'FILE-6.txt'),
            null,
            true
        );

        $client->submit($form, array(
            'file_form[title]' => 'Phalcon',
            'file_form[description]' => 'Talking about Phalcon PHP Framework',
            'file_form[location]' => $file
        ));

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

    }


    public function testApiSearchAllFilesSQL() {

        $client = static::createClient();
        $container = $client->getContainer();

        // All Files
        $link = '/file/search?v=0.1&file_search_form[type]=&file_search_form[createdAt]=&file_search_form[searchType]=SQL&file_search_form[title]=';
        $crawler = $client->request('GET', $link, array(), array(), array('X-Requested-With' => 'XMLHttpRequest'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent());
        $this->assertEquals(6, count($data->filesList));
        $this->assertEquals('success', $data->result);

        // Plain Text Files
        $link = '/file/search?v=0.1&file_search_form[type]=1&file_search_form[createdAt]=&file_search_form[searchType]=SQL&file_search_form[title]=';
        $crawler = $client->request('GET', $link, array(), array(), array('X-Requested-With' => 'XMLHttpRequest'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent());
        $this->assertEquals(4, count($data->filesList));
        $this->assertEquals('success', $data->result);

        // Markdown files
        $link = '/file/search?v=0.1&file_search_form[type]=7&file_search_form[createdAt]=&file_search_form[searchType]=SQL&file_search_form[title]=';
        $crawler = $client->request('GET', $link, array(), array(), array('X-Requested-With' => 'XMLHttpRequest'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent());
        $this->assertEquals(2, count($data->filesList));
        $this->assertEquals('success', $data->result);

        // Title = 'Symfony'
        $link = '/file/search?v=0.1&file_search_form[type]=&file_search_form[createdAt]=&file_search_form[searchType]=SQL&file_search_form[title]=Symfony';
        $crawler = $client->request('GET', $link, array(), array(), array('X-Requested-With' => 'XMLHttpRequest'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, count($data->filesList));
        $this->assertEquals('success', $data->result);

    }
}
