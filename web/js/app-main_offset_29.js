'use strict';
/**
 * @ngdoc service
 * @name angular-custom-utils:offsetFilter
 *
 * @description
 *
 *
 * */
angular.module('angular-custom-utils')
    .filter('offset', function(){
        return function(input, start) {
            start = parseInt(start, 10);
            return input.slice(start);
        };
});