'use strict';
/**
 * @ngdoc service
 * @name angular-custom-utils:capitalizeFilter
 *
 * @description
 * Capitalize String... Making upper first character of string
 *
 * */
angular.module('angular-custom-utils', []);

angular.module('angular-custom-utils')
    .filter('capitalize', function() {
        return function(input, all) {
            var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
            return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
        };
    });