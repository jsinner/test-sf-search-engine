/**
 * @ngdoc service
 * @name angular-custom-utils:httpInterceptorFactory
 *
 * @author José Gabriel González Pérez <jgonza67@cantv.com.ve>
 *
 * @description
 * Este interceptor (una vez añadido como interceptor del servicio $http mediante el $httpProvider)
 * me permitirá manejar los estados de las peticiones XMLHTTPRequest. Acá por ejemplo colocaré el "loading..."
 * cuando es enviada una petición XMLHTTPRequest o también podré mostrar más detalles de errores cuando
 * del lado del servidor ocurra un error.
 *
 * */
angular.module('angular-custom-utils')
    .factory('httpInterceptorFactory', ['$q', '$log', function($q, $log){

    var httpInterceptor = {

        /**
         * This method is called before $http sends the request to the backend
         *
         * @param config This function receives the request configuration object as a parameter and has to return a configuration object or a promise
         */
        'request': function (config) {
            // console.log(config);

            config.headers.common = [];

            config.headers.common['X-Requested-With'] = 'XMLHttpRequest';

            if(config.method.toUpperCase()=='POST') {
                config.headers.post = [];
                config.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            }
            else if(config.method.toUpperCase()=='PUT') {
                config.headers.put = [];
                config.headers.put["Content-Type"] = "application/x-www-form-urlencoded";
            }
            else if(config.method.toUpperCase()=='DELETE') {
                config.headers.delete = [];
                config.headers.delete["Content-Type"] = "application/x-www-form-urlencoded";
            }

            // Show the loading image
            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                Loading.show();
            }
            // console.log(config);
            return config;
        },

        /**
         * Request error interceptor captures requests that have been canceled by a previous request interceptor
         *
         * @param rejection
         */
        'requestError': function (rejection) {
            // Hide the Loading image
            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                Loading.hide();
            }
            jQuery.gritter.add({
                title: 'Ha ocurrido un error algo raro!',
                text: 'Ha ocurrido un error al intentar realizar una comunicación con el Servidor, recargue la página e inténtelo de nuevo. Si el problema persiste, comuniquese con el Administrador del Sistema (CIDS) e infórmele de los detalles para su corrección.',
                class_name: 'gritter-error',
                time: 7000
            });
            // console.log(rejection);
            return $q.reject(rejection);
        },

        /**
         * This method is called right after $http receives the response from the backend
         *
         * @param response This function receives a response object as a parameter and has to return a response object or a promise
         */
        'response': function (response) {
            // Hide the Loading image
            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                Loading.hide();
            }
            return response;
        },

        /**
         * Sometimes our backend call fails. In those cases, response error interceptor can help us to recover the backend call.
         *
         * @param rejection
         */
        'responseError': function (rejection) {
            // Hide the Loading image
            if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
                Loading.hide();
            }

            var errorDataRaw = rejection.data;
            var errorData = rejection.data.substring(0, 50);
            var errorMessage = `${errorData}... Comuniquese con el Administrador del Sistema (CIDS) e infórmele de los detalles para su corrección.`;

            jQuery('.gritter-item-wrapper').remove();
            var gritter = jQuery.gritter.add({
                title: `Ha ocurrido un error ${rejection.status} en el Servidor!`,
                text: `${errorMessage} <a data-error-displayed='false' style='color: #FFFFFF; font-weight: bold; cursor: pointer; font-size: 12px;' onClick='if($(this).attr("data-error-displayed")=="true"){$(this).attr("data-error-displayed", "false");$("#ajaxServerErrorMessage").hide().addClass("hide");} else { $(this).attr("data-error-displayed", "true");$("#ajaxServerErrorMessage").show().removeClass("hide");}'>Ver detalle...</a><div id='ajaxServerErrorMessage' class='hide' style='display:none;'>${errorDataRaw}</div>`,
                class_name: 'gritter-error'
            });

            return $q.reject(rejection);
        }

    };
;
    return httpInterceptor;

}]);
