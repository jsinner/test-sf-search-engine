(function(){

    var view = {

        initEvents: function () {

            $("#btn-return-back").on('click', function(evt){
                evt.preventDefault();
                window.history.back();
            });

            $('#file_form_title').bind('keyup blur', function () {
                helper.keyAlphaNum(this, true, true);
            });

            $('#file_form_description').bind('keyup blur', function () {
                helper.keyTextarea(this, true);
            });

            $('#file_form_title, #file_form_description').bind('blur', function () {
                helper.clearField(this);
            });

            console.log("Config page done!");

        }

    };

    var service = {

    };

    pageSetUp();

    view.initEvents();

})();