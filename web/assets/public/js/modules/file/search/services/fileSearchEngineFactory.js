/**
 * @ngdoc service
 * @name acclink:acceleranceExpertUserFactory
 *
 * @author José Gabriel González <jgabrielsinner10@gmail.com>
 *     
 * @description
 * 
 * */
angular.module('searchEngine')
    .factory('fileSearchEngineFactory', ["$q", "$http", function($q, $http){

        var search = function(actionUrl, data){

            var serializedData = Object.keys(data).map(function(key) {
                var result = 'v=0.1';
                for(var field in data[key]){
                    result += '&'+key+'['+field+']' + '=' + encodeURIComponent(data[key][field]);
                }
                return result;
            }).join('&');

            console.log(data);
            console.log(serializedData);

            var defer = $q.defer();
            var promise = defer.promise;
            $http({
                method: "GET",
                url: actionUrl+'?'+serializedData,
                headers : { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With' : 'XMLHttpRequest'}
            }).then(function (response) {
                defer.resolve(response.data);
            }, function(errorData){
                defer.reject(errorData);
                var errCode = 'AEU-E00001';
                var errMessage = "An error occurred while trying to search files";
                console.log({
                    "errorCode": errCode,
                    "errorMessage": errMessage,
                    "errorData": errorData}
                );
            });
            return promise;
        };


        var service = {
            search: function (actionUrl, data) {
                return search(actionUrl, data);
            }
        };

        return service;
}]);
