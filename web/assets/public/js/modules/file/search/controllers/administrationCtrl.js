/**
 * @ngdoc controller
 * @name acceleranceExpert:administrationCtrl
 *
 * @author José Gabriel González <jgabrielsinner10@gmail.com>
 *
 * @description Controller for Accelerance User Administration Panel
 *
 * @requires $scope
 * */
angular.module('searchEngine')

    .controller('administrationCtrl', ['$scope', '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'fileSearchEngineFactory',

        function ($scope, $window, DTOptionsBuilder, DTColumnDefBuilder, fileSearchEngineFactory) {

            pageSetUp();

            var vm = this;

            vm.filesList = $window.filesList || [];
            vm.seletedAll = false;
            vm.selectedItems = [];

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }
            var todayStr = yyyy+'-'+mm+'-'+dd;

            $('#file_search_form_createdAt').datepicker({
                dateFormat : 'yyyy-mm-dd',
                format : 'yyyy-mm-dd',
                autoclose: true,
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>'
            });

            vm.file = {
                file_search_form: {
                    type: '1',
                    createdAt: '',
                    searchType: 'DQL'
                }
            };

            var responsiveHelper_dt_basic = undefined;

            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            vm.dtOptions = DTOptionsBuilder
                .newOptions({
                    select: true
                })
                // Active Responsive plugin
                .withOption('responsive', true)
                .withOption('order', [1, 'asc'])
                .withBootstrap()
                .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-xs-12 col-sm-6 text-right hidden-xs'l><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withPaginationType('full_numbers');

            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0).notSortable(),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5).notSortable()
            ];

            vm.generatePath = function (routeName, params){
                return Routing.generate(routeName, params);
            };

            vm.toggleCreateAtField = function () {
                vm.file.file_search_form.createdAt = (vm.file.file_search_form.createdAt!=='')?'':todayStr;
            };

            vm.pathToDownload = function (baseUrl, filename) {
                return 'http://' + baseUrl + filename;
            };

            vm.pageChangeHandler = function(pageNumber, modelName) {
                // some code here for changing page handler event
            };

            vm.getCreatedDate = function (createdAt) {
                return createdAt.split(" ")[0];
            };

            vm.resetSearchForm = function (){
                vm.file = {
                    file_search_form: {
                        type: '',
                        createdAt: '',
                        searchType: 'DQL'
                    }
                };
            };

            vm.changeSearchType = function () {
                var searchType = vm.file.file_search_form.searchType;
                if(searchType=='FS'){
                    vm.file.file_search_form.createdAt = '';
                    vm.file.file_search_form.title = '';
                    vm.file.file_search_form.description = '';
                }
            };

            vm.search = function () {

                var route = vm.generatePath('app_file_search', {});

                fileSearchEngineFactory.search(route, vm.file).then(function(response){

                    if(response.result=='success'){

                        vm.filesList = response.filesList;

                    }
                    else{
                        $.gritter.add({
                            title: 'Search status result',
                            text: response.message,
                            class_name: 'gritter-error',
                            time: 4000
                        });
                    }

                    // console.log(response);

                }).catch(function(err){
                    // console.log(err);
                });

            };

        }

    ]);