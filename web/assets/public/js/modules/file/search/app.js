'use strict';
(function () {
    /**
     * @author José Gabriel González <jgabrielsinner10@gmail.com>
     *
     * @type {angular.Module}
     */
    angular.module('searchEngine',
        [
            'ngAnimate', 'datatables',
            'datatables.select', 'datatables.bootstrap', 'angular-custom-utils',
            'datatables.colreorder'
        ]
    );

    angular.module('searchEngine')
        .constant('constants', (function() {
            return {

            };
        })());

    angular.module('searchEngine')
        .config(['$httpProvider', '$interpolateProvider', function($httpProvider, $interpolateProvider) {
            $httpProvider.interceptors.push('httpInterceptorFactory');
            $interpolateProvider.startSymbol('[[').endSymbol(']]');
        }]);

})();