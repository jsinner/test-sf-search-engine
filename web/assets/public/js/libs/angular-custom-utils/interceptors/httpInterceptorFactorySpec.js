describe('Service: angular-custom-utils.httpInterceptorFactory', function () {

    // load the service's module
    beforeEach(module('angular-custom-utils'));

    // instantiate service
    var service;

    //update the injection
    beforeEach(inject(function (_httpInterceptorFactory_) {
        service = _httpInterceptorFactory_;
    }));

    /**
     * @description
     * Sample test case to check if the service is injected properly
     * */
    it('should be injected and defined', function () {
        expect(service).toBeDefined();
    });
});
