Search-Engine Test Project
==========================

A Symfony project created on July 23, 2016, 2:35 am.

Requirements:

    - PHP 5.6+
    - PHPUnit 5.4.4
    - Composer
    - Nodejs
    - Bower

## 1.- Configuration:

For do this you need to run this commands in your terminal

    cd test-sf-search-engine
    cp app/config/parameters.yml.dist app/config/parameters.yml
    
Edit the configuration of the database connection (User, Password)

## 2.- Create the uploaded files directory (This directory is ignored by gitignore file):

    mkdir web/uploads

## 3.- Install dependencies:

For do this you need to run this commands in your terminal

    composer install
    
## 4.- Grant Correct Permissions:

For do this you need to run this commands in your terminal

    sudo setfacl -R -m u:www-data:rwx -m u:`whoami`:rwx var/cache var/logs var/sessions web/uploads
    sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx var/cache var/logs var/sessions web/uploads

## 5.- Create the database, first you need to create a database in PostgreSQL called "search_engine".

## 6.- Restore the database (Preferably PostgreSQL):

    php bin/console doctrine:schema:update
    
or
    
    pg_restore --dbname=search_engine --verbose app/Resources/doc/search-engine.backup  

## 7.- Initial data:

    php bin/console doctrine:fixtures:load

## 8.- Install assets:

    php bin/console assets:install --symlink web
    php bin/console fos:js-routing:dump
    php bin/console assetic:dump

## 9.- Run Server

    php bin/console server:run

Browse to the http://localhost:8000 URL.

## 10.- For UnitTest and Functional Test:

This action allow to save a few data for testing even testing over the Web UI

    phpunit

## 11.- For search by command application:

    php bin/console help sinner:search:file

Find in Filesystem:

    php bin/console sinner:search:file fs
    php bin/console sinner:search:file fs --content Angular
    php bin/console sinner:search:file fs --content Symfony
    
Find in Database:

    php bin/console sinner:search:file
    php bin/console sinner:search:file --title symfony
    php bin/console sinner:search:file db --content Angular
    
# Some images of the System

## Command line interface

![Search File Command Help](https://drive.google.com/uc?export=&id=0B4KxFw4ttXnjc29VTnJrb0hUTmc)

![Search File Command From File System By Content](https://drive.google.com/uc?export=&id=0B4KxFw4ttXnjQ3ZlT3IwRTYzY1E)

![Search File Command From Database](https://drive.google.com/uc?export=&id=0B4KxFw4ttXnjWWVfNENtQUZzd3M)

## UI interface

![File Uploader](https://drive.google.com/uc?export=&id=0B4KxFw4ttXnjTzBNWjg4VHUtWnc)

![Search Engine](https://drive.google.com/uc?export=&id=0B4KxFw4ttXnjNERKTGxKQXQzWDg)

![Search Engine With Search Arguments](https://drive.google.com/uc?export=&id=0B4KxFw4ttXnjS3VxazYzWkljRUk)
